/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.sql.*;


public abstract class dbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    private String usuario,contraseña,database,driver,url;

    public dbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registros, String usuario, String contraseña, String database, String driver, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.database = database;
        this.driver = driver;
        this.url = url;
    }
    
     public dbManejador() {
        this.database = "sql10720053"; // Nombre de la base de datos
        this.driver = "com.mysql.cj.jdbc.Driver";
        this.usuario = "sql10720053"; // Usuario de la base de datos
        this.contraseña = "rN2tIj28uF"; // Contraseña de la base de datos
        this.url = "jdbc:mysql://sql10.freesqldatabase.com:3306/" + this.database; // URL de conexión actualizada
        this.EsDriver();
    }

    
      public boolean EsDriver(){
    boolean exito = false;
    try{
        Class.forName(driver);
        exito = true;
        
    }catch(ClassNotFoundException e){
        System.out.println("Surgio un error " + e.getMessage());
        System.exit(-1);
        }
    return exito;
    }
      
    public void desconestar(){
        try{
        //validar que la conexion este abierta
        if(!this.conexion.isClosed()) this.conexion.close();
            
            
        }catch(SQLException e){
            System.out.println("Surgio un error al cerrar la conexion " +e.getMessage());
        }
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public boolean conectar(){
      boolean exito = false;
      try{
      this.setConexion(DriverManager.getConnection(url, usuario, contraseña));
      exito = true;
      } catch(SQLException e){
          System.out.println("Surgio un error " + e.getMessage());
      }
      return exito;
    }
    
}