/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author LEGION 5 15IMH05H
 */
public class Clientes {
    private int idClientes;
    private String numero;
    private String nombre;
    private String fecha;
    private String domicilio;
    private int status;

    public Clientes(int idClientes, String numero, String nombre, String fecha, String domicilio, int status) {
        this.idClientes = idClientes;
        this.numero = numero;
        this.nombre = nombre;
        this.fecha = fecha;
        this.domicilio = domicilio;
        this.status = status;
    }
    
        public Clientes() {
        this.idClientes = 0;
        this.numero = "";
        this.nombre = "";
        this.fecha = "";
        this.domicilio = "";
        this.status = 0;
    }

    public int getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(int idClientes) {
        this.idClientes = idClientes;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
}

