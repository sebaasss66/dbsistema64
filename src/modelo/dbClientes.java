/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.sql.*;
import java.util.ArrayList;
import java.util.ArrayList;

/**
 *
 * @author sebas
 */
public class dbClientes extends dbManejador implements Persistencia{
     public dbClientes(){
   
    super();
    }

    @Override
    public void insertar(Object object) throws Exception {
                Clientes cli = new Clientes();
        cli = (Clientes)object;
        String consulta = "";
        consulta = "Insert into clientes(numero,nombre,FechaDeNacimiento,domicilio,status)" + 
                "values(?,?,?,?,?)";
        
        if (this.conectar()){
        this.sqlConsulta = this.conexion.prepareStatement(consulta);
        this.sqlConsulta.setString(1,cli.getNumero());
        this.sqlConsulta.setString(2,cli.getNombre());
        this.sqlConsulta.setString(3,cli.getFecha());
        this.sqlConsulta.setString(4,cli.getDomicilio());
        this.sqlConsulta.setInt(5,cli.getStatus());
            this.sqlConsulta.executeUpdate();
            this.desconestar();
    }}

    @Override
    public void actualizar(Object object) throws Exception {
                        Clientes cli = new Clientes();
        cli = (Clientes) object;
        
        String consulta = "";
        consulta = "update clientes set numero = ?, nombre = ?, fechaDeNacimiento=?, domicilio =?"
                + "where idClientes = ? and status = 0";
        
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, cli.getNumero());
            this.sqlConsulta.setString(2, cli.getNombre());
            this.sqlConsulta.setString(3, cli.getFecha());
            this.sqlConsulta.setString(4, cli.getDomicilio());
            this.sqlConsulta.setInt(5, cli.getIdClientes());
            
            this.sqlConsulta.executeUpdate();
            this.desconestar();
    }}

    @Override
    public void habilitar(Object object) throws Exception {
                   Clientes cli = new Clientes();
        cli = (Clientes) object;
        
        String consulta = "";
        consulta = "update clientes set status = 0 where idClientes = ? and status = 1";
        
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setInt(1, cli.getIdClientes());
            this.sqlConsulta.executeUpdate();
            this.desconestar();
    }

  

   
    
}

    @Override
    public void deshabilitar(Object object) throws Exception {
          Clientes cli = new Clientes();
        cli = (Clientes) object;
        
        String consulta = "";
        consulta = "update Clientes set status = 1 where idClientes = ? and status = 0";
        
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setInt(1, cli.getIdClientes());
            this.sqlConsulta.executeUpdate();
            this.desconestar();
        //To change body of generated methods, choose Tools | Templates.
    }

    }

    @Override
    public boolean siExiste(int id) throws Exception {
               boolean exito = false;
        String consulta = "";
        consulta = "select * from clientes where idClientes = ? and status = 0";
        
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setInt(1, id);
            registros = this.sqlConsulta.executeQuery();
            if(registros.next())exito = true;
            this.desconestar();
        }
        return exito; //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList listaProductos = new ArrayList<Clientes>();
        String consulta = "select * from clientes where status = 0 order by codigo";
        
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            registros = this.sqlConsulta.executeQuery();
            
            while(registros.next()){
                Clientes cli = new Clientes();
                
                cli.setNumero(registros.getString("Numero"));
                cli.setNombre(registros.getString("nombre"));
                cli.setDomicilio(registros.getString("domicilio"));
                cli.setFecha(registros.getString("fecha"));
                cli.setIdClientes(registros.getInt("idClientes"));
                cli.setStatus(registros.getInt("status"));
                
                //agregar al arrayList
                listaProductos.add(cli);
            }
            
        }
        this.desconestar();
        return listaProductos;
        //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public Object buscar(String codigo) throws Exception {
         Clientes cli = new Clientes();
        String consulta = "select * from clientes where codigo = ? and status =0";
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
             String numero = null;
            this.sqlConsulta.setString(1, numero);
            
            registros = this.sqlConsulta.executeQuery();
            
            if(registros.next()){
                cli.setNumero(registros.getString("numero"));
                cli.setNombre(registros.getString("nombre"));
                cli.setDomicilio(registros.getString("domicilio"));
                cli.setFecha(registros.getString("fecha"));
                cli.setIdClientes(registros.getInt("idClientes"));
                cli.setStatus(registros.getInt("status"));
                
            }
            
        }
        this.desconestar();
        return cli;
        //To change body of generated methods, choose Tools | Templates.

    }

    
}
    
   