package controlador;

import vista.jifClientes;
import modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import java.sql.SQLException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author sebas
 */
public class Controlador2 implements ActionListener {

    private jifClientes vista;
    private dbClientes db;
    private boolean EsActualizar;
    private int idClientes = 0;

    public Controlador2(jifClientes vista, dbClientes db) {
        this.vista = vista;
        this.db = db;

        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        this.deshabilitar();
    }

    // helpers
    public void limpiar() {
        vista.txtNum.setText("");
        vista.txtNombre.setText("");
        vista.dtFechaNacimiento.setDate(new Date());
        vista.txtDomicilio.setText("");
        vista.txtStatus.setText("");
    }

    public void cerrar() {
        int res = JOptionPane.showConfirmDialog(vista, "¿Desea cerrar el sistema?", "Clientes", JOptionPane.YES_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        if (res == JOptionPane.YES_OPTION) {
            vista.dispose();
        }
    }

    public void habilitar() {
        vista.txtNum.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtDomicilio.setEnabled(true);
        vista.txtStatus.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnNuevo.setEnabled(false);
    }

    public void deshabilitar() {
        vista.txtNum.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtDomicilio.setEnabled(false);
        vista.txtStatus.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnNuevo.setEnabled(true);
    }

    public boolean validar() {
        boolean exito = true;

        if (vista.txtNum.getText().equals("") || vista.txtNombre.getText().equals("")
                || vista.txtDomicilio.getText().equals("")) {
            exito = false;
        }

        return exito;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.btnLimpiar) {
            this.limpiar();
        }
        if (ae.getSource() == vista.btnCancelar) {
            this.limpiar();
            this.deshabilitar();
        }
        if (ae.getSource() == vista.btnCerrar) {
            this.cerrar();
        }
        if (ae.getSource() == vista.btnNuevo) {
            this.habilitar();
            this.EsActualizar = false;
        }
        if (ae.getSource() == vista.btnGuardar) {
            // validar guardar
            if (this.validar()) {
                Clientes cli = new Clientes();
                cli.setNumero(vista.txtNum.getText());
                cli.setNombre(vista.txtNombre.getText());
                cli.setFecha(this.convertirAñoMesDia(vista.dtFechaNacimiento.getDate()));
                cli.setDomicilio(vista.txtDomicilio.getText());
                cli.setStatus(0);

                try {
                    if (!this.EsActualizar) {
                        // agregar a un nuevo cliente
                        db.insertar(cli);
                        JOptionPane.showMessageDialog(vista, "Se agregó con éxito al cliente");
                    } else {
                        cli.setIdClientes(idClientes);
                        db.actualizar(cli);
                        JOptionPane.showMessageDialog(vista, "Se actualizó con éxito al cliente");
                    }
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(vista, "Surgió un error al interactuar con la base de datos: " + e.getMessage());
                    e.printStackTrace();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(vista, "Surgió un error: " + e.getMessage());
                    e.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(vista, "Faltaron datos de capturar");
            }
        }
    }

    public void IniciarVista() throws Exception {
        Locale.setDefault(new Locale("en", "US"));
        vista.setTitle("Clientes");
        vista.setSize(700, 800);
        vista.setVisible(true);
        try {
            this.ActualizarTabla(db.lista());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(vista, "Surgió un error: " + e.getMessage());
        }
    }

    public String convertirAñoMesDia(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }

    public void convertirStringDate(String fecha) {
        try {
            // convertir la cadena de texto a un objeto date
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.dtFechaNacimiento.setDate(date);
        } catch (ParseException e) {
            System.err.print(e.getMessage());
        }
    }

    public void ActualizarTabla(ArrayList<Clientes> arr) {
        String campos[] = { "Idclientes", "Numero", "Nombre", "FechaDeNacimiento", "Domicilio" };

        String[][] datos = new String[arr.size()][5];
        int reglon = 0;
        for (Clientes registro : arr) {
            datos[reglon][0] = String.valueOf(registro.getIdClientes());
            datos[reglon][1] = String.valueOf(registro.getNumero());
            datos[reglon][2] = String.valueOf(registro.getNombre());
            datos[reglon][3] = String.valueOf(registro.getFecha());
            datos[reglon][4] = String.valueOf(registro.getDomicilio());
            reglon++;
        }
        DefaultTableModel tb = new DefaultTableModel(datos, campos);
        vista.lista.setModel(tb);
    }
}